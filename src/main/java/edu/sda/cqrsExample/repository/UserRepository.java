package edu.sda.cqrsExample.repository;

import edu.sda.cqrsExample.model.User;

import java.util.HashMap;
import java.util.Map;

public class UserRepository {

    Map<Integer, User> users = initUserRepo();

    private Map<Integer, User> initUserRepo() {
        Map<Integer, User> userMap = new HashMap<>();

        User u1 = new User(1, "Varga Csenge", "v.csenge", "1234", false, "12345", 0);
        User u2 = new User(2, "Ionescu Ion", "iion", "abcd", false, "45679862", 0);
        User u3 = new User(3, "Popescu Ana", "anaaa", "12as", false, "123878643", 0);

        userMap.put(1, u1);
        userMap.put(2, u2);
        userMap.put(3, u3);

        return userMap;
    }

    public Map<Integer, User> getUsers() {
        return users;
    }

}
