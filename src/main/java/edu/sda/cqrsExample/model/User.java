package edu.sda.cqrsExample.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class User {

    @Id
    @GeneratedValue
    private int id;

    private String name;

    private String userName;

    private String password;

    private boolean isPremium;

    private String cardNumber;

    private int bonusPoints;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isPremium() {
        return isPremium;
    }

    public void setPremium(boolean premium) {
        isPremium = premium;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public int getBonusPoints() {
        return bonusPoints;
    }

    public void setBonusPoints(int bonusPoints) {
        this.bonusPoints = bonusPoints;
    }

    public User(int id, String name, String userName, String password, boolean isPremium, String cardNumber, int bonusPoints) {
        this.id = id;
        this.name = name;
        this.userName = userName;
        this.password = password;
        this.isPremium = isPremium;
        this.cardNumber = cardNumber;
        this.bonusPoints = bonusPoints;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", userName='" + userName + '\'' +
                ", password='" + password + '\'' +
                ", isPremium=" + isPremium +
                ", cardNumber='" + cardNumber + '\'' +
                ", bonusPoints=" + bonusPoints +
                '}';
    }
}
