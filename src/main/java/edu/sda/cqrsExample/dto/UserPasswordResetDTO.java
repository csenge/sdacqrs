package edu.sda.cqrsExample.dto;

public class UserPasswordResetDTO {

    private int id;

   private String newPassword;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public UserPasswordResetDTO(int id, String newPassword) {
        this.id = id;
        this.newPassword = newPassword;
    }
}
