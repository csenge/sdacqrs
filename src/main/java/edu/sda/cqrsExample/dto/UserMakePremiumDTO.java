package edu.sda.cqrsExample.dto;

public class UserMakePremiumDTO {
    private int id;

    private boolean isPremium;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isPremium() {
        return isPremium;
    }

    public void setPremium(boolean premium) {
        isPremium = premium;
    }

    public UserMakePremiumDTO(int id) {
        this.id = id;
    }
}
