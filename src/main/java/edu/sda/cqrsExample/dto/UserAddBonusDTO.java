package edu.sda.cqrsExample.dto;

public class UserAddBonusDTO {

    private int id;

    private int bonusPoints;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getBonusPoints() {
        return bonusPoints;
    }

    public void setBonusPoints(int bonusPoints) {
        this.bonusPoints = bonusPoints;
    }

    public UserAddBonusDTO(int id, int bonusPoints) {
        this.id = id;
        this.bonusPoints = bonusPoints;
    }
}
