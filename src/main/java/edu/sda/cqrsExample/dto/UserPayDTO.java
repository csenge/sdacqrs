package edu.sda.cqrsExample.dto;

public class UserPayDTO {

    private int id;

    private String password;

    private String cardNumber;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public UserPayDTO(int id, String password, String cardNumber) {
        this.id = id;
        this.password = password;
        this.cardNumber = cardNumber;
    }
}
