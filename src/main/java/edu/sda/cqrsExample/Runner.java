package edu.sda.cqrsExample;

import edu.sda.cqrsExample.dto.UserAddBonusDTO;
import edu.sda.cqrsExample.dto.UserMakePremiumDTO;
import edu.sda.cqrsExample.dto.UserPasswordResetDTO;
import edu.sda.cqrsExample.dto.UserPayDTO;
import edu.sda.cqrsExample.model.User;
import edu.sda.cqrsExample.service.UserCommandServiceImpl;
import edu.sda.cqrsExample.service.UserQueryServiceImpl;

public class Runner {

    public static void main(String[] args) {

        UserCommandServiceImpl commandService = new UserCommandServiceImpl();
        UserQueryServiceImpl queryService = new UserQueryServiceImpl();

        queryService.countAllUsers();
        queryService.getAllUsers();

        User u4 = new User(4, "Alandala Alin", "alyn", "90ab78", false, "197642345", 0);

        commandService.registerUser(u4);

        UserAddBonusDTO addBonusDTO = new UserAddBonusDTO(1, 3);
        commandService.addBonus(addBonusDTO);

        UserMakePremiumDTO makePremiumDTO = new UserMakePremiumDTO(2);
        commandService.makeUserPremium(makePremiumDTO);

        UserPasswordResetDTO passwordResetDTO = new UserPasswordResetDTO(3, "newPass");
        commandService.resetPassword(passwordResetDTO);

        UserPayDTO payDTO = new UserPayDTO(4, "90ab78", "197642345");
        commandService.pay(payDTO);

        queryService.getAllUsers();
        queryService.countAllUsers();
    }
}
