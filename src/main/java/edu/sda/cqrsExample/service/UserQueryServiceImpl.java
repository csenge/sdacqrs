package edu.sda.cqrsExample.service;

import edu.sda.cqrsExample.model.User;
import edu.sda.cqrsExample.repository.UserRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class UserQueryServiceImpl implements UserQueryService {

    public List<User> getAllUsers() {
        UserRepository userRepository = new UserRepository();
        Map<Integer, User> result = userRepository.getUsers();
        List<User> userList = new ArrayList<>();

        for (Integer u : result.keySet()) {
            System.out.println(result.get(u).toString());
            userList.add(result.get(u));
        }
       return userList;
    }

    public int countAllUsers() {
        UserRepository userRepository = new UserRepository();

        int result = userRepository.getUsers().size();
        System.out.println("There are " + result + "users registered in the system");
        return result;
    }
}
