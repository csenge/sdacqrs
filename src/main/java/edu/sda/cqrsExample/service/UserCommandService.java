package edu.sda.cqrsExample.service;

import edu.sda.cqrsExample.dto.UserAddBonusDTO;
import edu.sda.cqrsExample.dto.UserMakePremiumDTO;
import edu.sda.cqrsExample.dto.UserPasswordResetDTO;
import edu.sda.cqrsExample.dto.UserPayDTO;
import edu.sda.cqrsExample.model.User;

public interface UserCommandService {

    void addBonus(UserAddBonusDTO bonusDTO);

    void makeUserPremium(UserMakePremiumDTO premiumDTO);

    void resetPassword(UserPasswordResetDTO passwordResetDTO);

    void pay(UserPayDTO payDTO);

    void registerUser(User user);
}
