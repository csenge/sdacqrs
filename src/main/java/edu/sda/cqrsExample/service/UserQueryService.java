package edu.sda.cqrsExample.service;

import edu.sda.cqrsExample.model.User;

import java.util.List;

public interface UserQueryService {

    List<User> getAllUsers();
    int countAllUsers();
}
