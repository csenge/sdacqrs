package edu.sda.cqrsExample.service;

import edu.sda.cqrsExample.dto.UserAddBonusDTO;
import edu.sda.cqrsExample.dto.UserMakePremiumDTO;
import edu.sda.cqrsExample.dto.UserPasswordResetDTO;
import edu.sda.cqrsExample.dto.UserPayDTO;
import edu.sda.cqrsExample.model.User;
import edu.sda.cqrsExample.repository.UserRepository;

import java.util.Map;

public class UserCommandServiceImpl implements UserCommandService {

    private static UserRepository userRepository = new UserRepository();

    public void addBonus(UserAddBonusDTO bonusDTO) {
        Map<Integer, User> userMap = userRepository.getUsers();

        User userToUpdate = userMap.get(bonusDTO.getId());
        userToUpdate.setBonusPoints(bonusDTO.getBonusPoints());

        userMap.put(bonusDTO.getId(), userToUpdate);
    }

    public void makeUserPremium(UserMakePremiumDTO premiumDTO) {
        Map<Integer, User> userMap = userRepository.getUsers();

        User userToUpdate = userMap.get(premiumDTO.getId());
        userToUpdate.setPremium(true);

        userMap.put(premiumDTO.getId(), userToUpdate);
    }

    public void resetPassword(UserPasswordResetDTO passwordResetDTO) {
        Map<Integer, User> userMap = userRepository.getUsers();

        User userToUpdate = userMap.get(passwordResetDTO.getId());
        userToUpdate.setPassword(passwordResetDTO.getNewPassword());

        userMap.put(passwordResetDTO.getId(), userToUpdate);
    }

    public void pay(UserPayDTO payDTO) {
        Map<Integer, User> userMap = userRepository.getUsers();

        System.out.println("User has payed with card having the cardnumber: " + payDTO.getCardNumber() + " and the password" + payDTO.getPassword());
    }

    public void registerUser(User user) {
        Map<Integer, User> userMap = userRepository.getUsers();

        userMap.put(user.getId(), user);
    }
}
